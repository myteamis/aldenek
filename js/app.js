console.log('hello')

//cart
let cartIcon = document.querySelector('#card-icon');
let cart = document.querySelector('.cart');
let cartClose = document.querySelector('#close-cart');
//open
cartIcon.onclick = () => {
    cart.classList.add('active-cart');
    console.log('cliked');
};
//close
cartClose.onclick = () => {
    cart.classList.remove('active-cart');
    console.log('cliked');
};

//catalog 
    let catalogIcon = document.querySelector('#open-catalog');
    let catalog = document.querySelector('.catalog');
    let catalogClose = document.querySelector('#close-catalog');

//open
    catalogIcon.onclick = () => {
    catalog.classList.add('active-catalog');
};
//close
    catalogClose.onclick = () => {
    catalog.classList.remove('active-catalog');
};


//tabs on catalog
let tabsBtn = document.querySelectorAll('.catalog-info');
let catalogColection = document.querySelectorAll('.tab');

console.log(catalogColection);

tabsBtn.forEach((element) => {
    element.addEventListener('click', function (){
        let currentBtn = element;
        let tabId = currentBtn.getAttribute('data-tab');
        let currentTab = document.querySelector(tabId);
        console.log(tabId);
        console.log(currentTab);
       
        tabsBtn.forEach((element) => {
            element.classList.remove("active");
        });

        catalogColection.forEach((element) => {
            element.classList.remove("active-tab");
        });


        currentBtn.classList.add("active");
        currentTab.classList.add("active-tab");
        
    })
});



//cart working
if(document.readyState == "loading"){
    document.addEventListener("DOMContentLoaded", ready);
}else{
    ready();
}

//making function
function ready(){
    //remove item from cart
    let removeCartButton = document.getElementsByClassName("cart-remove");
    for(let i = 0; i < removeCartButton.length; i++){
        let button = removeCartButton[i];
        button.addEventListener("click", removeCartItem);    
    }

    //quantity change
    let quantityInputs = document.getElementsByClassName('cart-quantity');
    for(let i = 0; i < quantityInputs.length; i++){
        let input = quantityInputs[i];
        input.addEventListener('change', quantityChanged);
    }

    //add to cart
    let addCart = document.getElementsByClassName("add-cart");
    for(let i = 0; i < addCart.length; i++){
        let button = addCart[i];
        button.addEventListener("click", addCartCliked);      
    }

    //buy batton work
    document.getElementsByClassName("btn-buy")[0].addEventListener("click", buyButtonCliked);
}

//buy batton clicked
function buyButtonCliked(){
    alert("Ваш заказ выполнен");
    let cartContent = document.getElementsByClassName("cart-content")[0];
    while(cartContent.hasChildNodes()){
        cartContent.removeChild(cartContent.firstChild);
    }
    updateTotal();
}

//remove item from cart
function removeCartItem(event){
    let buttonCliked = event.target;
    buttonCliked.parentElement.remove();
    updateTotal();
}

//quantity change
function quantityChanged(event){
    let input = event.target;
    if(isNaN(input.value) || input.value <= 0){
        input.value = 1;
    }
    updateTotal();
}

//add to cart
function addCartCliked(event){
    let button = event.target;
    let shopProducts = button.parentElement;
    let title = shopProducts.getElementsByClassName("product-title")[0].innerText;
    let price = shopProducts.getElementsByClassName("price")[0].innerText;
    let img = shopProducts.getElementsByClassName("product-img")[0].src;
    addProductToCart(title, price, img);
    updateTotal();
}

function addProductToCart(title, price, img){
    debugger;
    let cartShopeBox = document.createElement("div");
    cartShopeBox.classList.add("cart-box");
    let cartItems = document.getElementsByClassName("cart-content")[0];
    let cartItemsNames = cartItems.getElementsByClassName("cart-product-title");
    for(let i = 0; i < cartItemsNames.length; i++){
        if(cartItemsNames[i].innerText == title){
            alert("ЭТА ВЕЩЬ УЖЕ У ВАС В КОРЗИНЕ =)");
            return;
        }
    }
    let cartBoxContent = ` <img src="${img}" alt="" class="cart-img">
                            <div class="detail-box">
                            <div class="cart-product-title">${title}</div>
                            <div class="cart-price">${price}</div>
                            <input type="number" value="1" class="cart-quantity">
                            </div>
                            <!--remove-->
                            <i class='bx bxs-trash cart-remove'></i>`;
    cartShopeBox.innerHTML = cartBoxContent;
    cartItems.append(cartShopeBox);
    cartShopeBox.getElementsByClassName('cart-remove')[0].addEventListener('click', removeCartItem);
    cartShopeBox.getElementsByClassName('cart-quantity')[0].addEventListener('change', quantityChanged);
}


//update total
function updateTotal(){
    let cartContent = document.getElementsByClassName('cart-content')[0];
    let cartBoxes = document.getElementsByClassName('cart-box');
    let total = 0;

    for(let i = 0; i < cartBoxes.length; i++){
        let cartBox = cartBoxes[0];
        let priceElement = cartBox.getElementsByClassName("cart-price")[0];
        let quantityElement = cartBox.getElementsByClassName("cart-quantity")[0];
        let price = parseFloat(priceElement.innerText.replace("руб.",""));
        let quantity = quantityElement.value;
        total = total + (price * quantity);
    }
    document.getElementsByClassName("total-price")[0].innerText = "руб." + total;
}


