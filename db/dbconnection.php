<?php
    $connect_data = "host=localhost port=5432 dbname=production user=postgres password=1234";
    $db_connect = pg_connect($connect_data); // подключение, возвращает true/false
    
    if (!$db_connect) {
        die("Ошибка подключения: " . pg_result_error());
    }
    echo "Подключение к БД прошло успешно.";
    
    pg_close($connect_data);
    
?>