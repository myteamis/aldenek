-- Table: basis.t_discount_type

-- DROP TABLE IF EXISTS basis.t_discount_type;

CREATE TABLE IF NOT EXISTS basis.t_discount_type
(
    id integer NOT NULL DEFAULT nextval('basis.t_discount_type_id_seq'::regclass),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    into_param numeric(18,2) NOT NULL,
    coeff numeric(18,2) NOT NULL,
    CONSTRAINT t_discount_type_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS basis.t_discount_type
    OWNER to postgres;

GRANT ALL ON TABLE basis.t_discount_type TO _developer;

GRANT ALL ON TABLE basis.t_discount_type TO postgres;
-- Index: t_discount_type_idx1

-- DROP INDEX IF EXISTS basis.t_discount_type_idx1;

CREATE UNIQUE INDEX IF NOT EXISTS t_discount_type_idx1
    ON basis.t_discount_type USING btree
    (name COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;