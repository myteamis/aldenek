-- Table: basis.t_client

-- DROP TABLE IF EXISTS basis.t_client;

CREATE TABLE IF NOT EXISTS basis.t_client
(
    id integer NOT NULL DEFAULT nextval('basis.t_client_id_seq'::regclass),
    user_login character varying COLLATE pg_catalog."default",
    password character varying COLLATE pg_catalog."default",
    sname character varying COLLATE pg_catalog."default",
    name character varying COLLATE pg_catalog."default",
    pname character varying COLLATE pg_catalog."default",
    discount_type_id integer DEFAULT 0,
    CONSTRAINT t_client_pkey PRIMARY KEY (id),
    CONSTRAINT t_client_discount_type_id_fkey FOREIGN KEY (discount_type_id)
        REFERENCES basis.t_discount_type (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS basis.t_client
    OWNER to postgres;

GRANT ALL ON TABLE basis.t_client TO _developer;

GRANT ALL ON TABLE basis.t_client TO postgres;
-- Index: t_client_idx1

-- DROP INDEX IF EXISTS basis.t_client_idx1;

CREATE UNIQUE INDEX IF NOT EXISTS t_client_idx1
    ON basis.t_client USING btree
    (name COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;