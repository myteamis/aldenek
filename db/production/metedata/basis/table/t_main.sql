-- Table: basis.t_main

-- DROP TABLE IF EXISTS basis.t_main;

CREATE TABLE IF NOT EXISTS basis.t_main
(
    id integer NOT NULL DEFAULT nextval('basis.t_main_id_seq'::regclass),
    category_id integer NOT NULL,
    CONSTRAINT t_main_pkey PRIMARY KEY (id),
    CONSTRAINT t_main_category_id_fkey FOREIGN KEY (category_id)
        REFERENCES basis.t_category (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS basis.t_main
    OWNER to postgres;

GRANT ALL ON TABLE basis.t_main TO _developer;

GRANT ALL ON TABLE basis.t_main TO postgres;
-- Index: t_main_idx1

-- DROP INDEX IF EXISTS basis.t_main_idx1;

CREATE INDEX IF NOT EXISTS t_main_idx1
    ON basis.t_main USING btree
    (category_id ASC NULLS LAST)
    TABLESPACE pg_default;