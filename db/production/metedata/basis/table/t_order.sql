-- Table: basis.t_order

-- DROP TABLE IF EXISTS basis.t_order;

CREATE TABLE IF NOT EXISTS basis.t_order
(
    id integer NOT NULL DEFAULT nextval('basis.t_order_id_seq'::regclass),
    client_id integer NOT NULL,
    good_id integer NOT NULL,
    count integer,
    cost numeric(18,2),
    CONSTRAINT t_order_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS basis.t_order
    OWNER to postgres;

GRANT ALL ON TABLE basis.t_order TO _developer;

GRANT ALL ON TABLE basis.t_order TO postgres;
-- Index: t_order_idx1

-- DROP INDEX IF EXISTS basis.t_order_idx1;

CREATE INDEX IF NOT EXISTS t_order_idx1
    ON basis.t_order USING btree
    (good_id ASC NULLS LAST)
    TABLESPACE pg_default;