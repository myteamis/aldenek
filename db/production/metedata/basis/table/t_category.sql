-- Table: basis.t_category

-- DROP TABLE IF EXISTS basis.t_category;

CREATE TABLE IF NOT EXISTS basis.t_category
(
    id integer NOT NULL DEFAULT nextval('basis.t_category_id_seq'::regclass),
    name character varying COLLATE pg_catalog."default",
    CONSTRAINT t_category_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS basis.t_category
    OWNER to postgres;

GRANT ALL ON TABLE basis.t_category TO _developer;

GRANT ALL ON TABLE basis.t_category TO postgres;
-- Index: t_category_idx1

-- DROP INDEX IF EXISTS basis.t_category_idx1;

CREATE UNIQUE INDEX IF NOT EXISTS t_category_idx1
    ON basis.t_category USING btree
    (name COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;