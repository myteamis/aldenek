-- Table: basis.t_good

-- DROP TABLE IF EXISTS basis.t_good;

CREATE TABLE IF NOT EXISTS basis.t_good
(
    id integer NOT NULL DEFAULT nextval('basis.t_good_id_seq'::regclass),
    name character varying COLLATE pg_catalog."default",
    category_id integer NOT NULL,
    cost numeric(18,2),
    CONSTRAINT t_good_pkey PRIMARY KEY (id),
    CONSTRAINT t_good_category_id_fkey FOREIGN KEY (category_id)
        REFERENCES basis.t_category (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS basis.t_good
    OWNER to postgres;

GRANT ALL ON TABLE basis.t_good TO _developer;

GRANT ALL ON TABLE basis.t_good TO postgres;
-- Index: t_good_idx1

-- DROP INDEX IF EXISTS basis.t_good_idx1;

CREATE INDEX IF NOT EXISTS t_good_idx1
    ON basis.t_good USING btree
    (category_id ASC NULLS LAST)
    TABLESPACE pg_default;