-- FUNCTION: basis.fn_order_create(integer, jsonb)

-- DROP FUNCTION IF EXISTS basis.fn_order_create(integer, jsonb);

CREATE OR REPLACE FUNCTION basis.fn_order_create(
	IN arg_client_id integer,
    IN arg_json_data jsonb 
)RETURNS boolean
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $BODY$
declare 
    v_rec record;
    v_discount numeric(18,2);
begin
    if coalesce(arg_client_id,0) = 0 then
        raise exception 'ERROR Не указан id клиента!';
    end if;
    
    select coeff
    into v_discount
    from basis.t_client cl
    join basis.t_discount_type dt ON
        dt.id = cl.discount_type_id
    limit 1;

    for v_rec in 
        select * 
        from json_to_record(arg_json_data) j (
            good_id integer,
            count integer
        )
    loop 
    
        insert into basis.t_order(
            client_id,
            good_id,
            count,
            cost
        ) values (
            arg_client_id,
            v_rec.good_id,
            v_rec.count,
            (g.cost * v_rec.count * v_discount)
        );

    end loop;

    return true;
end;
$BODY$;

ALTER FUNCTION basis.fn_order_create(integer, jsonb)
    OWNER TO postgres;

GRANT EXECUTE ON FUNCTION basis.fn_order_create(integer, jsonb) TO postgres;

COMMENT ON FUNCTION basis.fn_order_create(integer, jsonb)
    IS 'Создание заказа';
