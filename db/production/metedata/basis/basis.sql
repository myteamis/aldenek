-- SCHEMA: basis

-- DROP SCHEMA IF EXISTS basis ;

CREATE SCHEMA IF NOT EXISTS basis
    AUTHORIZATION pg_database_owner;

COMMENT ON SCHEMA basis
    IS 'basis schema';

GRANT USAGE ON SCHEMA basis TO PUBLIC;

GRANT ALL ON SCHEMA basis TO pg_database_owner;