-- View: basis.v_export_stat

-- DROP VIEW basis.v_export_stat;

CREATE OR REPLACE VIEW basis.v_export_stat
 AS
 SELECT DISTINCT c.id,
    concat_ws(' '::text, c.sname, c.name, c.pname) AS client_fio,
    sum(o.cost) OVER () AS client_cost,
    dt.name,
        CASE
            WHEN dt.id <> 4 THEN dt.into_param - sum(o.cost) OVER ()
            ELSE 0::numeric
        END AS to_next
   FROM basis.t_client c
     LEFT JOIN basis.t_order o ON o.client_id = c.id
     JOIN basis.t_discount_type dt ON dt.id = c.discount_type_id;

ALTER TABLE basis.v_export_stat
    OWNER TO _developer;
COMMENT ON VIEW basis.v_export_stat
    IS 'Экспорт. Заказы';

GRANT SELECT ON TABLE basis.v_export_stat TO PUBLIC;
GRANT ALL ON TABLE basis.v_export_stat TO _developer;

