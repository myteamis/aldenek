-- SEQUENCE: basis.t_main_id_seq

-- DROP SEQUENCE IF EXISTS basis.t_main_id_seq;

CREATE SEQUENCE IF NOT EXISTS basis.t_main_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1
    OWNED BY t_main.id;

ALTER SEQUENCE basis.t_main_id_seq
    OWNER TO postgres;